# README #

Hermes is a tool to manage your orders, then track your delivery.

V2: migrate to Django 2 and Python 3.

### Features ###

* Customer address management
* Purchase order/payment tracking
* Delivery tracking, update progress twice a day for all your delivery
* Multi accounts support
