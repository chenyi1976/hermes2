from django.contrib.auth.models import User
from django.db import models
from django.db.models import CASCADE
from django.utils.timezone import now


class PersonGroup(models.Model):
    name = models.CharField('群名', max_length=6)

    def __str__(self):
        return '{}: {}'.format(self.id, self.name)


class Person(models.Model):
    user = models.ForeignKey(User, blank=True, null=True, on_delete=CASCADE)
    name = models.CharField('名字', max_length=6)
    level = models.CharField('排序', max_length=15, blank=True, null=True, default='')
    address = models.TextField('地址', max_length=200, blank=True, null=True)
    description = models.TextField('注释', max_length=200, blank=True, null=True, default='')
    proof = models.CharField('身份证号码', max_length=18, blank=True, null=True)
    phone = models.CharField('电话号码', max_length=40, blank=True, null=True)
    image_front = models.ImageField('身份证正面', blank=True, null=True)
    image_back = models.ImageField('身份证反面', blank=True, null=True)
    group = models.ForeignKey(PersonGroup, verbose_name='群组', blank=True, null=True, on_delete=CASCADE)

    def __str__(self):
        return '{}: {}'.format(self.id, self.name)


class Stuff(models.Model):
    name = models.CharField('英文名', max_length=200)
    name_c = models.CharField('中文名', max_length=200)
    brand = models.CharField('品牌', max_length=40)
    level = models.CharField('排序', max_length=15, blank=True, null=True, default='')
    price = models.DecimalField('价格', max_digits=6, decimal_places=2, default=0)
    dose = models.CharField('剂量', max_length=100, blank=True, null=True)
    barcode = models.CharField('条形码', max_length=50, blank=True, null=True)
    image = models.ImageField('图片', blank=True, null=True)
    comment = models.CharField('注释', max_length=1000, blank=True, null=True)

    def __str__(self):
        return '{}: {}/{} '.format(self.id, self.name_c, self.name)


class Courier(models.Model):
    name = models.CharField(max_length=200)
    detail = models.TextField(max_length=800, blank=True, null=True)
    serial_regex = models.TextField(max_length=200, blank=True, null=True)
    rank = models.PositiveIntegerField(default=0, blank=True, null=True)
    status_url = models.TextField(max_length=400, blank=True, null=True)
    url_fields = models.TextField(max_length=400, blank=True, null=True)
    is_post = models.BooleanField(default=True)
    xpath_date = models.TextField(max_length=400, blank=True, null=True)
    xpath_location = models.TextField(max_length=400, blank=True, null=True)
    xpath_detail = models.TextField(max_length=400, blank=True, null=True)

    def __str__(self):
        return '{}: {}'.format(self.id, self.name)


class Order(models.Model):
    user = models.ForeignKey(User, blank=True, null=True, on_delete=CASCADE)
    person = models.ForeignKey(Person, verbose_name='顾客', on_delete=CASCADE)
    started = models.DateField('日期', auto_now_add=True, blank=True, null=True)
    price = models.DecimalField('总价', max_digits=6, decimal_places=2, default=0, blank=True, null=True)
    paid = models.BooleanField('已付款？', default=False)
    received = models.BooleanField('已收货？', default=False)
    detail = models.TextField('详单', max_length=1000, blank=True, null=True)

    def __str__(self):
        return '{}: {} {} '.format(self.id, self.person.name, self.detail[:10])


class Delivery(models.Model):
    user = models.ForeignKey(User, blank=True, null=True, on_delete=CASCADE)
    order = models.ForeignKey(Order, verbose_name='订单', on_delete=CASCADE)
    courier = models.ForeignKey(Courier, verbose_name='快递', blank=True, null=True, on_delete=CASCADE)
    serial = models.CharField('单号', max_length=50, blank=True, null=True)
    started = models.DateField('开始日期', default=now)
    image = models.ImageField('快递单照片', blank=True, null=True)
    comment = models.CharField('注释', max_length=200, blank=True, null=True)

    def __str__(self):
        return '{}: {}'.format(self.id, self.serial)

    class Meta:
        unique_together = ('user', 'order', 'serial')


class DeliveryStatus(models.Model):
    delivery = models.ForeignKey(Delivery, verbose_name='快递', on_delete=CASCADE)
    date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    d_date = models.CharField('更新日期', max_length=50)
    d_location = models.CharField('地点', max_length=50)
    d_detail = models.CharField('详情', max_length=50)

    def __str__(self):
        return '{}: {} / {}'.format(self.id, self.delivery.serial, self.date)


class AppKey(models.Model):
    user = models.ForeignKey(User, on_delete=CASCADE)
    app = models.CharField('App', max_length=50)
    key = models.CharField('Key', max_length=50)

    def __str__(self):
        return '{}: {} / {} / {}'.format(self.id, self.user.username, self.app, self.key)
