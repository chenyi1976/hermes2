import logging
import time
from nntplib import NNTPReplyError

import requests
from django.core.management import BaseCommand
from lxml import html

from converse.models import Order, DeliveryStatus


def suck_web(deliveries):
    retry_max_count = 3
    result = {}

    for delivery in deliveries:

        delivery_serial = delivery.serial
        if delivery_serial[:2] == "SS" or delivery_serial[:2] == "BS" \
                or delivery_serial[:2] == "SD" or delivery_serial[:3] == "TYS":
            page_url = "http://track.blueskyexpress.com.au/cgi-bin/GInfo.dll?EmmisTrack"
            data = {'w': "aaeexpress", "cno": delivery_serial}
            xpath_date = "//table[@id='oTHtable']//tr[last()]/td[1]"
            xpath_location = "//table[@id='oTHtable']//tr[last()]/td[2]"
            xpath_detail = "//table[@id='oTHtable']//tr[last()]/td[3]"
        elif delivery_serial[:3] == "SHL" or delivery_serial[:3] == "SCY":
            page_url = "https://www.shlexp.com/track.html"
            data = {'cno': delivery_serial}
            xpath_date = "//div[@id='oDetail']//tr[last()]/td[1]"
            xpath_location = "//div[@id='oDetail']//tr[last()]/td[2]"
            xpath_detail = "//div[@id='oDetail']//tr[last()]/td[3]"
        elif len(delivery_serial) == 13:
            page_url = "http://kd.hitaoe.com/main?wtl=shipTrack&code=" + delivery_serial
            data = {}
            xpath_date = "//tbody//tbody/tr[last()]/td[1]"
            xpath_location = "//tbody//tbody/tr[last()]/td[3]"
            xpath_detail = "//tbody//tbody/tr[last()]/td[2]"
        elif len(delivery_serial) == 12 or delivery_serial[:1] == "A":
            page_url = "http://www.zhexpress.com.au/TOrderQuery_Service.aspx"
            data = {"OrderId": delivery_serial}
            xpath_date = "//tbody[@class='ig_LaytonItem igg_LaytonItem']/tr[last()]/td[1]/span"
            xpath_location = "//tbody[@class='ig_LaytonItem igg_LaytonItem']/tr[last()]/td[2]/span"
            xpath_detail = "//tbody[@class='ig_LaytonItem igg_LaytonItem']/tr[last()]/td[3]/span"
        elif len(delivery_serial) == 10:
            page_url = "http://www.auodexpress.com/website/doAuodList.do"
            data = {"waybillNum": delivery_serial}
            xpath_date = "//table[@id='logs']//table/tr[1]/td[1]"
            xpath_location = "//table[@id='logs']//table/tr[1]/td[2]"
            xpath_detail = "//table[@id='logs']//table/tr[1]/td[2]"

        # response = requests.post(page_url, data={'w':"aaeexpress", "cno": "BS103138"})
        response = None
        retry_count = 0
        while response is None and retry_count < retry_max_count:

            # sleep 3 seconds.
            time.sleep(3)
            retry_count += 1

            try:
                if data:
                    response = requests.post(page_url, data)
                else:
                    response = requests.get(page_url)
            except:
                pass

        if response is None:
            logging.debug('fail: url can not be opened for {}'.format(delivery_serial))
            continue

        the_page = response.text

        tree = html.fromstring(the_page)

        try:
            d_date = tree.xpath(xpath_date)[0].text
            if d_date:
                d_date.strip()
            d_location = tree.xpath(xpath_location)[0].text
            if d_location:
                d_location.strip()
            d_detail = tree.xpath(xpath_detail)[0].text
            if d_detail:
                d_detail.strip()
            result[delivery] = (d_date, d_location, d_detail)
            logging.debug(delivery_serial, d_date, d_location, d_detail)
        except NNTPReplyError:
            logging.debug('NNTPReplyError: can not get data for given xpath for {}'.format(delivery_serial))
        except IndexError:
            logging.debug('IndexError: can not get data for given xpath for {}'.format(delivery_serial))
        except AttributeError:
            logging.debug('AttributeError: can not get data for given xpath for {}'.format(delivery_serial))
        except Exception:
            logging.debug('fail: can not get data for given xpath for {}'.format(delivery_serial))

    return result


class Command(BaseCommand):
    help = 'Pull Delivery Status'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        logging.debug('You are now pulling Delivery Status.')
        orders = Order.objects.filter(received=False)
        deliveries = set()
        for order in orders:
            for delivery in order.delivery_set.all():
                deliveries.add(delivery)

        data_dict = suck_web(deliveries)

        logging.debug('{} deliveries, {} successful queried.'.format(len(deliveries), len(data_dict)))
        for delivery, data in data_dict.items():
            status = DeliveryStatus()
            status.delivery_id = delivery.id
            status.d_date = data[0]
            status.d_location = data[1]
            status.d_detail = data[2]
            status.save()
        logging.debug('You are now pulling Delivery Status.')
