from django.core.management import call_command
from django.test import TestCase

from converse.models import Person, Order, Delivery


class SuckTestCase(TestCase):
    delivery_id = "SHL7761776"

    def setUp(self):
        person = Person.objects.create(name='sean')
        order = Order.objects.create(person=person)
        Delivery.objects.create(order=order, serial=self.delivery_id)

    def test_pull_delivery_status(self):
        "Test command: pull_delivery_status. "

        call_command('pull_delivery_status')

        status = Delivery.objects.filter(serial=self.delivery_id)[0].deliverystatus_set.all()[0]
        self.assertIsNotNone(status)
