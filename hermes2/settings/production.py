from .defaults import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
ALLOWED_HOSTS = ['45.79.101.50', '127.0.0.1']
