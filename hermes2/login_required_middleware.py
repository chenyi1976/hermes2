from re import compile

from django.http import HttpResponseRedirect
from django.utils.deprecation import MiddlewareMixin

from . import settings

EXEMPT_URLS = [compile(settings.LOGIN_URL.lstrip('/'))]
if hasattr(settings, 'LOGIN_EXEMPT_URLS'):
    EXEMPT_URLS += [compile(expr) for expr in settings.LOGIN_EXEMPT_URLS]


class LoginRequiredMiddleware(MiddlewareMixin):

    @staticmethod
    def process_request(request):
        assert hasattr(request, 'user'), "LoginRequiredMiddleware"
        if not request.user.is_authenticated:
            path = request.path_info.lstrip('/')
            if not any(m.match(path) for m in EXEMPT_URLS):
                return HttpResponseRedirect(settings.LOGIN_URL + '?next=' + settings.LOGIN_REDIRECT_URL)
